#!/bin/sh
#echo "start port forwarding..."

# forward 7199 ports in cassandra nodes to localhost
#sshpass -p root ssh -4 -oStrictHostKeyChecking=no -fN -L 127.0.0.1:17199:172.17.0.1:7199 root@172.17.0.1
#sleep 5
#sshpass -p root ssh -4 -oStrictHostKeyChecking=no -fN -L 127.0.0.1:27299:172.17.0.2:7299 root@172.17.0.2
#sleep 5

#echo "done port forwarding..."

#echo "start iptable forwarding..."

# route cassandra hosts ip addresses to localhost
#iptables -t nat -A OUTPUT -p all -d 172.17.0.1 -j DNAT --to-destination 127.0.0.1
#iptables -t nat -A OUTPUT -p all -d 172.17.0.2 -j DNAT --to-destination 127.0.0.1

#echo "done iptable forwarding..."

exec /usr/local/bin/entrypoint.sh cassandra-reaper "$@"
