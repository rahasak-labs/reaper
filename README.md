# reaper

## orchestrate repairs with cassandra-reaper 

- read more from [here](https://medium.com/@itseranga/orchestrate-repairs-with-cassandra-reaper-26094bdb59f6).


## cassandra repairs on ssl enabled cluster

- read more from [here](https://medium.com/rahasak/cassandra-repairs-with-ssl-enabled-cluster-62410bb1114f)


## cassandra-reaper in sidecar mode

- read more from [here](https://medium.com/rahasak/cassandra-reaper-in-sidecar-mode-6be754f7c97e)


## monitor cassandra-reaper with prometheus and grafana

- read more from [here](https://medium.com/@itseranga/monitor-cassandra-reaper-with-prometheus-and-grafana-83fd4ccc88ef)
