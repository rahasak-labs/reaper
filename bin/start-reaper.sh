#!/bin/bash

# exit on error
set -e

# start cassandra-reaper
sudo service cassandra-reaper start
