#!/bin/bash

# exit on error
set -e

# configure /etc/cassandra-reaper/cassandra-reaper.yaml
REAPER_CONFIG_FILE="/etc/cassandra-reaper/cassandra-reaper.yaml"
echo "start configuring ${REAPER_CONFIG_FILE}"

# set storageType cassandra
# storageType: cassandra
sudo sed -ri 's/(storageType:).*/\1 '"cassandra"'/' "${REAPER_CONFIG_FILE}"

# set reaper SIDECAR
# datacenterAvailability: SIDECAR
sudo sed -ri 's/(datacenterAvailability:).*/\1 '"SIDECAR"'/' "${REAPER_CONFIG_FILE}"

# set JMX port to 7199
# 127.0.0.1: 7199
JMX_PORT=7199
sudo sed -i "s/7100/${JMX_PORT}/g" "${REAPER_CONFIG_FILE}"

# add cassandra conf to end
# cassandra:
#   clusterName: reaper-cluster
#   contactPoints: ["172.17.0.1"]
#   port: 9042
#   keyspace: reaper_db
#   loadBalancingPolicy:
#     type: tokenAware
#     shuffleReplicas: true
#     subPolicy:
#       type: dcAwareRoundRobin
#       localDC:
#       usedHostsPerRemoteDC: 0
#       allowRemoteDCsForLocalConsistencyLevel: false
#   ssl:
#     type: jdk
sudo sed -i -e '$a\
\
cassandra:\
\ \ clusterName: reaper-cluster\
\ \ contactPoints: [\"hostname\"]\
\ \ port: 9042\
\ \ keyspace: reaper_db\
\ \ loadBalancingPolicy:\
\ \ \ \ type: tokenAware\
\ \ \ \ shuffleReplicas: true\
\ \ \ \ subPolicy:\
\ \ \ \ \ \ type: dcAwareRoundRobin\
\ \ \ \ \ \ localDC:\
\ \ \ \ \ \ usedHostsPerRemoteDC: 0\
\ \ \ \ \ \ allowRemoteDCsForLocalConsistencyLevel: false\
\ \ ssl:\
\ \ \ \ type: jdk\
' "${REAPER_CONFIG_FILE}"

# host ip address as seed
CASSANDRA_SEED=`hostname --ip-address`
sudo sed -i "s/hostname/${CASSANDRA_SEED}/g" "${REAPER_CONFIG_FILE}"

echo "done configuring ${REAPER_CONFIG_FILE}"


# configure /usr/local/bin/cassandra-reaper
REAPER_STARTUP_FILE="/usr/local/bin/cassandra-reaper"
echo "start configuring ${REAPER_STARTUP_FILE}"

# set JAVA_OPTS with keystore/truststore locations and passwords
# JAVA_OPTS="-Djavax.net.ssl.keyStore=/etc/ssl/cassandra-server-keystore.jks -Djavax.net.ssl.keyStorePassword=cassandra -Djavax.net.ssl.trustStore=/etc/ssl/generic-server-truststore.jks -Djavax.net.ssl.trustStorePassword=cassandra"

# exec java ${JVM_OPTS[@]} \
#     ${JAVA_OPTS} \
#     -cp ${CLASS_PATH} \
#     io.cassandrareaper.ReaperApplication \
#     server ${CONFIG_PATH}
sudo sed -i '/^exec java.*/i JAVA_OPTS=\"-Djavax.net.ssl.keyStore=/etc/ssl/cassandra-server-keystore.jks -Djavax.net.ssl.keyStorePassword=cassandra -Djavax.net.ssl.trustStore=/etc/ssl/generic-server-truststore.jks -Djavax.net.ssl.trustStorePassword=cassandra\"' "${REAPER_STARTUP_FILE}"
sudo sed -i '/^exec java.*/a ${JAVA_OPTS} \\' "${REAPER_STARTUP_FILE}"

echo "done configuring ${REAPER_STARTUP_FILE}"
