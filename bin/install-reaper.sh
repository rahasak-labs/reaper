#!/bin/bash

# exit on error
set -e

# install cassandra-reaper
echo "deb https://dl.bintray.com/thelastpickle/reaper-deb wheezy main" | sudo tee -a /etc/apt/sources.list
sudo apt-key adv --keyserver keyserver.ubuntu.com --recv-keys 2895100917357435
sudo apt-get install -y apt-transport-https
sudo apt-get update
sudo apt-get install -y reaper
